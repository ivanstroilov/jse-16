package ru.t1.stroilov.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Wrong Login or Password...");
    }

}
