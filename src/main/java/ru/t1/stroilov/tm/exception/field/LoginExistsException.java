package ru.t1.stroilov.tm.exception.field;

public final class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
