package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.User;

public interface IUserRepository extends IRepository<User> {


    User findByLogin(String login);

    User findByEmail(String email);


    Boolean loginExists(String login);

    Boolean emailExists(String email);
}
