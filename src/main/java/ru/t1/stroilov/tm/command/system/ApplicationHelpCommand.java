package ru.t1.stroilov.tm.command.system;

import ru.t1.stroilov.tm.api.model.ICommand;
import ru.t1.stroilov.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    public final static String DESCRIPTION = "Display list of terminal commands.";

    public final static String NAME = "help";

    public final static String ARGUMENT = "-h";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            System.out.println(command.toString());
        }
    }
}
