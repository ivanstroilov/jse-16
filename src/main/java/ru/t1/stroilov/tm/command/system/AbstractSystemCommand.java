package ru.t1.stroilov.tm.command.system;

import ru.t1.stroilov.tm.api.service.ICommandService;
import ru.t1.stroilov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
